/**
 * Page containing list of coins favorited by user
 */
import React, { Component } from "react";
import { connect } from "react-redux";

import CoinList from "../components/CoinList";
import * as actions from "../store/actions";

export class Favorites extends Component {
    componentDidMount() {
        this.props.onGetFavorites();
    }

    render() {
        const favorites = this.props.favorites.slice(0);

        return (
            <div>
                <CoinList coins={favorites} currency={this.props.currency} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { favorites: state.favorites, currency: state.currency };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetFavorites: () => dispatch(actions.getFavorites()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
