/**
 * Navigation bar to switch between Dashboard and Favorites page.
 * Allows user to switch between currencies
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";

export class NavBar extends Component {
    handleClickTab = (path) => {
        this.props.history.push(path);
    };

    render() {
        const tabStyles = {
            fontSize: "18px",
            fontWeight: "bold",
            color: "#282c34",
        };

        return (
            <nav className="navbar navbar-expand-md ">
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="nav nav-tabs card-header-tabs">
                        <li className="btn-group" role="group">
                            <button
                                className="btn btn-light"
                                // disable button if Favorites tab is active
                                disabled={this.props.location.pathname === "/"}
                                onClick={() => this.handleClickTab("/")}
                                style={tabStyles}
                            >
                                DASHBOARD
                            </button>

                            <button
                                className="btn btn-light"
                                // disable button if Favorites tab is active
                                disabled={
                                    this.props.location.pathname ===
                                    "/favorites"
                                }
                                onClick={() =>
                                    this.handleClickTab("/favorites")
                                }
                                style={tabStyles}
                            >
                                FAVORITES
                            </button>
                        </li>
                    </ul>
                </div>

                <div
                    className="btn-group"
                    role="group"
                    aria-label="Select currency"
                >
                    <button
                        type="button"
                        className="btn btn-info"
                        disabled={this.props.currency === "usd"}
                        onClick={() => this.props.onClick("usd")}
                    >
                        USD
                    </button>
                    <button
                        type="button"
                        className="btn btn-info"
                        disabled={this.props.currency === "myr"}
                        onClick={() => this.props.onClick("myr")}
                    >
                        MYR
                    </button>
                    <button
                        type="button"
                        className="btn btn-info"
                        disabled={this.props.currency === "btc"}
                        onClick={() => this.props.onClick("btc")}
                    >
                        BTC
                    </button>
                </div>
            </nav>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currency: state.currency,
    };
};

export default connect(mapStateToProps, null)(withRouter(NavBar));
