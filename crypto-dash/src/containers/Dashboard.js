/**
 * List at least 20 coins
 * (Price, volume, name, symbol of coins)
 * Graph of 7 days data from Sparkline API
 */
import React, { Component } from "react";
import { connect } from "react-redux";

import CoinList from "../components/CoinList";
class Dashboard extends Component {
    render() {
        const coins = this.props.coins.slice(0, 50); // list first 50 coins

        return <CoinList coins={coins} currency={this.props.currency} />;
    }
}

const mapStateToProps = (state) => {
    return {
        // Retrieve props from reducer
        coins: state.coins,
        currency: state.currency,
    };
};

export default connect(mapStateToProps, null)(Dashboard);
