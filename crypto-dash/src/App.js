import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";

import Dashboard from "./containers/Dashboard";
import Favorites from "./containers/Favorites";
import NavBar from "./containers/NavBar";
import * as actions from "./store/actions";

import "./App.css";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currency: this.props.currency, // currency of coin, default: usd
        };
    }

    // called after elements of page rendered correctly
    componentDidMount() {
        // Dispatch action to retrieve coin market data
        this.props.onGetCoins(this.state.currency);
    }

    // Change value of currency state when button clicked
    handleCurrChange = (curr) => {
        if (this.state.currency !== curr) {
            console.log(
                `Changing currency from ${this.state.currency} to ${curr}`
            );
            this.setState({ currency: curr });
            this.props.onChangeCurr(curr);
        } else {
            console.log(`Already using ${curr}`);
        }
    };

    render() {
        return (
            <BrowserRouter>
                <div className="App-header">Cryptocurrency Market</div>
                <div className="App">
                    <NavBar
                        currency={this.state.currency}
                        onClick={this.handleCurrChange}
                    />
                    <Switch>
                        <Route exact path="/" component={Dashboard} />
                        <Route exact path="/favorites" component={Favorites} />
                        <Route
                            render={() => <h1>This page does not exist.</h1>}
                        />
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currency: state.currency,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetCoins: (curr) => dispatch(actions.getCoins(curr)),
        onChangeCurr: (curr) => dispatch(actions.changeCurr(curr)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
