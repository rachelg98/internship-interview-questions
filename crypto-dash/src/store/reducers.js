import * as actionType from "./actionTypes";

const initialState = {
    coins: [],
    currency: "usd",
    favorites: [],
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.CHANGE_CURR:
            // update price and volume of coins to new currency
            state.coins.forEach((coin) => {
                const newCoin = action.coins.find((obj) => obj.id === coin.id);
                coin.price = newCoin.current_price;
                coin.volume = newCoin.total_volume;
            });
            const newCurrState = {
                ...state,
                coins: state.coins,
                favorites: state.favorites,
                currency: action.currency,
            };
            return newCurrState;

        case actionType.GET_COINS:
            // retrieve list of coin objects
            return { ...state, coins: action.coins };

        case actionType.GET_FAVORITES:
            // retrieve list of favorited coins
            const newState = {
                ...state,
                favorites: state.coins.filter(function (elem) {
                    return elem.isFavorited === true;
                }),
            };
            return newState;

        case actionType.ADD_FAVORITE:
            // Set isFavorited flag of selected coin as true
            const coinToFavorite = state.coins.find(
                (obj) => obj.id === action.id
            );
            coinToFavorite.isFavorited = true;
            return state;

        case actionType.REMOVE_FAVORITE:
            // Set isFavorited flag of selected coin as false
            const coinToDelete = state.coins.find(
                (obj) => obj.id === action.id
            );
            coinToDelete.isFavorited = false;
            // remove coin with given id from favorites list
            return {
                ...state,
                favorites: state.favorites.filter(function (elem) {
                    return elem.id !== action.id;
                }),
            };

        default:
            return state;
    }
};

export default rootReducer;
