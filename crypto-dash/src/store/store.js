/**
 * Store configured based on Redux documentation
 */

import { applyMiddleware, compose, createStore } from "redux";
// import thunkMiddleware from "redux-thunk";
// import monitorReducerEnhancer from "./enhancers/monitorReducer";
// import loggerMiddleware from "./middleware/logger";
import thunk from "redux-thunk";
import { routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";

import rootReducer from "./reducers";

export default function configureStore(preloadedState) {
    // Add custom middleware to enable async action calls
    // const middlewares = [loggerMiddleware, thunkMiddleware];
    // const middlewareEnhancer = applyMiddleware(...middlewares);
    const history = createBrowserHistory();
    const middlewares = [thunk, routerMiddleware(history)];
    const composeEnhancers =
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(
        rootReducer,
        composeEnhancers(applyMiddleware(...middlewares))
    );

    // const enhancers = [middlewareEnhancer, monitorReducerEnhancer];
    // const composeEnhancers = compose(...enhancers);

    // const store = createStore(rootReducer, preloadedState, composeEnhancers);

    return store;
}
