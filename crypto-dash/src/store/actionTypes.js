export const CHANGE_CURR = "CHANGE_CURR";
export const GET_COINS = "GET_COINS";
export const ADD_FAVORITE = "ADD_FAVORITE";
export const REMOVE_FAVORITE = "REMOVE_FAVORITE";
export const GET_FAVORITES = "GET_FAVORITES";
