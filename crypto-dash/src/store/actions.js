import * as actionType from "./actionTypes";
import axios from "axios";

export const changeCurr_ = (newCoins, newCurr) => {
    return {
        type: actionType.CHANGE_CURR,
        coins: newCoins,
        currency: newCurr,
    };
};

// Retrieve market data for coins in new currency
export const changeCurr = (newCurr) => {
    return (dispatch) => {
        return axios
            .get("https://api.coingecko.com/api/v3/coins/markets", {
                params: { vs_currency: newCurr },
            })
            .then((res) => {
                dispatch(changeCurr_(res.data, newCurr));
            });
    };
};

export const getCoins_ = (data) => {
    // create array of coin objects from query data
    const coins = data.map((coin) => {
        return {
            id: coin.id,
            image: coin.image,
            name: coin.name,
            price: coin.current_price,
            symbol: coin.symbol,
            sparkline: coin.sparkline_in_7d,
            volume: coin.total_volume,
            // flag for whether coin has been added to favorites
            isFavorited: false, // default value
        };
    });
    return {
        type: actionType.GET_COINS,
        coins: coins,
    };
};

export const getCoins = (currency) => {
    return (dispatch) => {
        return axios
            .get("https://api.coingecko.com/api/v3/coins/markets", {
                params: { vs_currency: currency, sparkline: true },
            })
            .then((res) => {
                dispatch(getCoins_(res.data));
            });
    };
};

export const getFavorites = () => {
    return {
        type: actionType.GET_FAVORITES,
    };
};

// Mark coin with given id as favorited
export const addFavorite = (id) => {
    return {
        type: actionType.ADD_FAVORITE,
        id: id,
    };
};

//
export const removeFavorite = (id) => {
    return {
        type: actionType.REMOVE_FAVORITE,
        id: id,
    };
};
