/**
 * Display line graph of coin sparklines for recent 7 days using Recharts API.
 * Receives sparkline Object as props from CoinList component,
 * containing double price array as dataset
 */
import React from "react";
import { LineChart, Line, Tooltip, YAxis } from "recharts";

export default function Graph(props) {
    const data = props.sparkline.price.map((coin) => {
        return {
            value: coin,
        };
    });

    return (
        <LineChart
            data={data}
            width={400}
            height={80}
            margin={{ top: 5, right: 30, bottom: 5, left: 5 }}
        >
            <Line
                dataKey="value"
                dot={false}
                stroke="grey"
                type="monotone"
            ></Line>
            <Tooltip />
            <YAxis
                hide={true}
                scaleToFit={true}
                orientation="right"
                type="number"
                domain={["dataMin", "dataMax"]} // adjust range of y-axis
                unit="1"
            />
        </LineChart>
    );
}
