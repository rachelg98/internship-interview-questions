/**
 * Presentational component for list of coins
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../store/actions";
import Graph from "./Graph";
import currSymbol from "../constants/currency";
import "./CoinList.css";

class CoinList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            coins: this.props.coins,
        };
    }

    componentDidUpdate(prevProps) {
        // Check for changes in props
        if (prevProps.coins !== this.props.coins) {
            // update state to new props
            this.setState({ coins: this.props.coins });
        }
    }

    handleClickFavorite = (coin) => {
        if (!coin.isFavorited) {
            // Add selected coin object to favorites list
            this.props.onAddFavorite(coin.id);
        } else {
            // If already favorited remove from favorites list
            this.props.onRemoveFavorite(coin.id);
        }
        // update state
        this.setState({ coins: this.props.coins });
    };

    render() {
        const coinList = this.state.coins.map((coin) => (
            <tr key={coin.id}>
                <td>
                    <img alt={coin.name} src={coin.image} />
                </td>
                <td id="name">
                    {coin.name}
                    <div>
                        <small className="text-muted">{coin.symbol}</small>
                    </div>
                </td>
                <td id="price">
                    {currSymbol[this.props.currency]} {coin.price}
                </td>
                <td id="volume">{coin.volume}</td>
                <td>
                    <Graph sparkline={coin.sparkline} />
                </td>
                <td>
                    <button
                        className={`btn ${
                            coin.isFavorited
                                ? "btn-danger"
                                : "btn-outline-danger"
                        }`}
                        onClick={() => this.handleClickFavorite(coin)}
                    >
                        <span> &#9825;</span>
                    </button>
                </td>
            </tr>
        ));

        return (
            <div className="table-responsive{-sm|-md|-lg}">
                <table className="table table-light table-hover">
                    <thead className="thead-light">
                        <tr>
                            <th scope="col" colSpan="2">
                                Currency
                            </th>
                            <th scope="col">Price ({this.props.currency})</th>
                            <th scope="col">Volume</th>
                            <th scope="col-xs-6">Sparkline (7 days)</th>
                            <th scope="col-xs-1"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {coinList.length > 0 ? (
                            coinList
                        ) : (
                            <td colSpan="12">
                                Nothing to show at the moment...
                            </td>
                        )}
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onAddFavorite: (id) => dispatch(actions.addFavorite(id)),
        onRemoveFavorite: (id) => dispatch(actions.removeFavorite(id)),
    };
};

export default connect(null, mapDispatchToProps)(CoinList);
