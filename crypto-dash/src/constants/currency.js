// Array of currency symbols
const currSymbol = {
    usd: "$",
    myr: "RM",
    btc: "฿",
};

export default currSymbol;
