# Answers to Internship Interview Questions

1.  Please describe yourself using JSON (include your internship start/end date).

    > Refer to [JSON](./resume.json)

2.  Tell us about a newer (less than five years old) web technology you like and why?

    > A recent technology that has caught my interest is **Flutter**, a cross-platform UI development framework. The main attraction is that it enables developers to native applications on both Android and iOS while maintaining only a single codebase. Flutter's hot reload feature is also very convenient and makes it a lot easier to instantly view changes to code reflected in the app.

3.  In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?

    > We can use a LinkedList which enables users to add or remove items from it over time. For instance, to add a new item to the list we can simply create a new node and insert it into the desired position in the list. Suppose we have a doubly linked list with Node objects containing a single int item, an example of an add method is as follows:
    ```
    // Sample code to add new item to linked list at a given index using Java
    void add(int index, int num){
        Node newNode = new Node(num); // declare new node object
        if(head==null){ // add new node to empty list
            head = newNode;
            tail = newNode;
        } else if(index <= 0 ) { // insert item at head of list
            newNode.next = head;
            head.prev = newNode;
            head = newNode; // new head
        } else if(index >= length - 1) { // insert item at end of list
            newNode.prev = tail;
            tail.next = newNode;
            tail = newNode; // new tail
        } else { // insert node in middle of list
            Node curr = head; // pointer to head of node
            
            // traverse to given index
            while(index > 0 && curr.next != null){
                curr = curr.next; // move to next node
                index--; // decrement counter
            }
            // insert item after item in current position
            newNode.prev = curr.prev; // insert item
            newNode.next = curr;
            curr.prev.next = newNode;
            curr.prev = newNode;
        }
        // increment length of linked list
        length++;
    }
    ```
    > Adding and deleting data in a LinkedList has time complexity O(n) (or O(1) for data manipulation at head or tail) and is faster than an ArrayList since bits do not have to be shifted in memory. However updating or retrieving data at a given index runs in O(n) time compared to O(1) for Arraylist. 

4.  Explain this block of code in Big-O notation.

    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```

    > The above sampleCode method has a runtime of _*O( n<sup>2</sup> )*_, because of the nested _for_ loops.

5.  One of our developers built a page to display market information for over 500 cryptocurrencies in a single page. After several days, our support team started receiving complaints from our users that they are not able to view the website and it takes too long to load. What do you think causes this issue and suggest at least 3 ideas to improve the situation.

    > The issue is likely caused by trying to load too many resources at once. Some possible solutions include:
    >
    > 1.  Use pagination to limit the number of cryptocurriences being displayed.
    > 2.  Reduce the number of DOM elements
    > 3.  HTTP comrpession

6.  In Javascript, What is a "closure"? How does JS Closure works?

    > Closure is the concept where an inner function has access to the variables defined in the function enclosing it by preserving its values. An example of closure can be seen as follows:

    ```
    function outer() {
        var closure = 2; // preserved value
        function inner() {
            var x = 2; // reinitialized each time
            console.log(`x=${x}\tclosure=${closure}`);
                x *= 2;
                closure *= 2;
            }
            return inner;
        }

        var double = outer();

       double(); // output: x = 2  closure=2
        double(); // output: x = 2  closure=4
    }
    ```

    > This is because each time the function finishes execution, variables within its scope will cease to exist. Evoking the _double_ function will reinitialize _x_. However, the value of variables in _outer_ is preserved as closure and continues to exist.

7.  In Javascript, what is the difference between var, let, and const. When should I use them?

    > _const_ variables cannot be reassigned after it has been defined. _const_ can be used when the user wants to declare constants whose values will not be changed later on. <br/><br/> _var_ and _let_ variables can be reassigned. _var_ is function scoped, while _let_ is block-scoped. Unlike _let_, _var_ variables enable hoisting and can be redeclared. _let_ should be used over _var_ to avoid scoping confusion.

8.  Share with us one book that has changed your perspective in life. How did it change your life?

    > One book that left a deep impression on me was "Sapiens" by Yuval Harari. It changed my outlook on humanity and modern society as a whole. One of the theories proposed in the book was that hunter gatherers lived happier lives than modern day man, and hence were much better off. It made me question the meaning of life. Perhaps our focus shouldn't be on the hedonism or the pursuit of happiness but on contentment in the present.

9.  What is the thing you believe in that is true, that most people disagree with?

    > Sometimes it's better to aim low. Although ambition can be a huge driving force in achieving greater goals, setting reasonable expectations is just as if not more important. Getting something done halfway is better than not doing it at all; and aiming for an achievable goal albeit low is better than aiming for the stars. Setting impractical goals makes procrastination all the more tempting and makes starting a task more daunting. Aiming low, achieving it and working towards the next goal makes progress a lot easier.

10. What are your thoughts on the subject of Arts and Humanities?

    > I think that pursuing knowledge in Arts & Humanities is essential for holistic development. As part of my college graduation requirements, I've taken quite a few Arts & Humanities courses (e.g. language, cultural and art) which I enjoyed thorouhly. It helps promote creativity and build soft skills outside of STEM subjects.
